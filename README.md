# CreativeTextures #

Here lives a modern, flat drop-in texture pack replacement for Minecraft 1.8+ developed with love for CreativeCraft.

## Screenshots ##

![morning.jpg](https://bitbucket.org/repo/XbRGkx/images/3640821227-morning.jpg)
![afternoon.jpg](https://bitbucket.org/repo/XbRGkx/images/553366316-afternoon.jpg)
![night.jpg](https://bitbucket.org/repo/XbRGkx/images/4193483959-night.jpg)